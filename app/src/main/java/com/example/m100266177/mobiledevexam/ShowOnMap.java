package com.example.m100266177.mobiledevexam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by 100266177 on 08/12/2015.
 */
public class ShowOnMap extends FragmentActivity {
    private GoogleMap map;
    private double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("EEEEEEEEEEEE", "in intent");
        setContentView(R.layout.show_on_map);


        Intent callingIntent = getIntent();
        latitude = callingIntent.getDoubleExtra("latitude", 0.0);
        longitude = callingIntent.getDoubleExtra("longitude", 0.0);

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        Log.e("EEEEEEEEEEEE", "in 2");

        if (map == null) {
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment)).getMap();
            if (map != null) {
                showMapLocation();
            }
        }
    }

    private void showMapLocation() {
        Log.e("EEEEEEEEEEEE", "in 3");

        LatLng position = new LatLng(latitude, longitude);
        map.addMarker(new MarkerOptions().position(position).title("Search Result"));
        map.animateCamera(CameraUpdateFactory.newLatLng(position));

        map.setTrafficEnabled(true);
        map.setBuildingsEnabled(true);
        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);
    }
}

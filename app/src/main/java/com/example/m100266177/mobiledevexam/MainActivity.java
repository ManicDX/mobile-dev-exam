package com.example.m100266177.mobiledevexam;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends Activity {

    Spinner spinner;
    EditText bikeShareIdView;
    EditText nameView;
    EditText addressView;
    EditText latitudeView;
    EditText longitudeView;
    EditText numBikesView;

    public String url = "http://www.bikesharetoronto.com/data/stations/bikeStations.xml";

    DataDownloadedListener dTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init views, all but name disabled
        spinner = (Spinner)findViewById(R.id.spinner);
        bikeShareIdView = (EditText)findViewById(R.id.bikeShareEditView);
        bikeShareIdView.setEnabled(false);
        nameView = (EditText)findViewById(R.id.nameEditView);
        addressView = (EditText)findViewById(R.id.addressEditView);
        addressView.setEnabled(false);
        latitudeView = (EditText)findViewById(R.id.latitudeEditView);
        latitudeView.setEnabled(false);
        longitudeView = (EditText)findViewById(R.id.longitudeEditView);
        longitudeView.setEnabled(false);
        numBikesView = (EditText)findViewById(R.id.numbikesEditView);
        numBikesView.setEnabled(false);

        //TODO init database stuff
        dTask = new DataDownloadedListener();
        dTask.execute(url);

        //TODO Add bikes from data base to spinner
        //ArrayList<String> names = new ArrayList<>();
        //for(int i = 0; i < dTask.bikes.size(); i++){
        //    names.add(dTask.bikes.get(i).getName());
        //}
        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
        //        android.R.layout.simple_spinner_item, names);
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //spinner.setAdapter(dataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mobile_dev_exam, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveButtonHandler(View view){
        //TODO updateBike, only really need name?
        //Bike bike = new Bike(bikeShareIdView.getText(), latitudeView.getText(), longitudeView.getText(),
        //                    nameView.getText(), numBikesView.getText(), addressView.getText());
        //dTask.bikeDatabase.updateBike();


    }

    public void showMapButtonHandler(View view){
        //TODO
        //Address ADDRESS = geocodeLookup(list.get(1));
//
        //Intent showMapIntent = new Intent(this, ShowOnMap.class);
//
        //showMapIntent.putExtra("location", list.get(1));
        //if (list.get(1) != null) {
        //    showMapIntent.putExtra("latitude", ADDRESS.getLatitude());
        //    showMapIntent.putExtra("longitude", ADDRESS.getLongitude());
        //}
        //startActivity(showMapIntent);
    }
}

package com.example.m100266177.mobiledevexam;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 100266177 on 08/12/2015.
 */
public class BikeDBHelper extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_FILENAME = "bikes.db";
    public static final String TABLE_NAME = "Bikes";

    public static final String  bikeID =                "_id";
    public static final String  bikeShareId =           "bikeShareId";
    public static final String  latitude =              "latitude";
    public static final String  longitude =             "longitude";
    public static final String  name =                  "name";
    public static final String  numBikes =              "numBikes";
    public static final String  address =               "address";

    // don't forget to use the column name '_id' for your primary key
    public static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" +
            " " + bikeID + " integer primary key autoincrement," +
            " " + bikeShareId + " int not null," +
            " " + latitude + " double not null," +
            " " + longitude + " double not null" +
            " " + name + " text not null" +
            " " + numBikes + " int not null" +
            " " + address + " text null" +
            ")";


    public static final String DROP_STATEMENT = "DROP TABLE " + TABLE_NAME;

    public BikeDBHelper(Context context) {
        super(context, DATABASE_FILENAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL(DROP_STATEMENT);
        database.execSQL(CREATE_STATEMENT);
    }

    //TODO database stuff
    public Bike createBike(int bikeShareId, double latitude, double longitude,
                    String name, int numBikes, String address){

        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // insert the data into the database
        ContentValues values = new ContentValues();
        values.put(this.bikeShareId, bikeShareId);
        values.put(this.name, name);
        values.put(this.address, address);
        values.put(this.latitude, latitude);
        values.put(this.longitude, longitude);
        values.put(this.numBikes, numBikes);
        long id = database.insert(TABLE_NAME, null, values);
        return new Bike(bikeShareId,latitude, longitude, name, numBikes, address);
    }

    public List<Bike> getAllBikes(){
        ArrayList<Bike> bikeList = new ArrayList<>();

        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // retrieve the contact from the database
        String[] columns = new String[] {bikeShareId, latitude, longitude, name, numBikes, address};
        Cursor cursor = database.query(TABLE_NAME, columns, "", new String[]{}, "", "", "");

        cursor.moveToFirst();
        do {
            // collect the contact data, and place it into a contact object
            long id = Long.parseLong(cursor.getString(0));
            String bikeShareID = cursor.getString(1);
            String latitude = cursor.getString(2);
            String longitude = cursor.getString(3);
            String name = cursor.getString(4);
            String numBikes = cursor.getString(5);
            String address = cursor.getString(6);
            Bike bike = new Bike(Integer.parseInt(bikeShareID), Double.parseDouble(latitude), Double.parseDouble(longitude),
                    name, Integer.parseInt(numBikes), address);
            bike.setId(id);;

            // add the current contact to the list
            bikeList.add(bike);

            // advance to the next row in the results
            cursor.moveToNext();
        } while (!cursor.isAfterLast());



        Log.i("DatabaseAccess", "queryDatabase():  num: " + bikeList.size());

        return bikeList;

    }

    public void updateBike(Bike bike){
        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(name, bike.getName());
        int numRowsAffect = database.update(name, values,"_name = ?", new String[]{"" + bike.getName()});
    }

    public void deleteAllBikes(){
        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // retrieve the contact from the database
        String[] columns = new String[] {bikeShareId, latitude, longitude, name, numBikes, address};
        Cursor cursor = database.query(TABLE_NAME, columns, "", new String[]{}, "", "", "");

        cursor.moveToFirst();
        do {

            int numRowsAffected = database.delete(TABLE_NAME, "_id = ?", new String[]{""});

        } while (!cursor.isAfterLast());

    }
}

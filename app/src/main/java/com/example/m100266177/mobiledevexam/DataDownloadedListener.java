package com.example.m100266177.mobiledevexam;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by 100266177 on 08/12/2015.
 */



public class DataDownloadedListener extends AsyncTask<String, Void, String> {

    //public void dataDownloaded(List<Bike> bikes);
    public List<Bike> bikes;
    String data = "";

    public BikeDBHelper bikeDatabase;

    Context context;

    @Override
    protected String doInBackground(String... params) {
        try
        {
            URL url = new URL(params[0]);
            HttpURLConnection conn = null;
            conn = (HttpURLConnection) url.openConnection();
            InputStream in = conn.getInputStream();
            //data = in.toString();
            data = getStringFromInputStream(in);
            //System.out.println(temp);

        }
        catch(IOException e)
        {
            e.printStackTrace();
            Log.e("Error", "No Connection");
        }

        //Check if getting data from server, success
        //Log.e("testing data retrieval", data);

        //TODO get context from main
        //context = MainActivity.class;
        //bikeDatabase = new BikeDBHelper(context);



        //TODO delete all bikes after download
        //bikeDatabase.deleteAllBikes();


        //TODO save bikes to database

        return data;
    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }
}
